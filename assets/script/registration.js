
let registerEmail = ""
let registerPassword = ""

const registerBtn = document.getElementById("registerBtn")
registerBtn.addEventListener('click', function(){
	const registerEmailValue = document.getElementById('registerEmail').value;
	const registerPasswordValue = document.getElementById("registerPassword").value;

	if(registerEmailValue === "" || registerPasswordValue === ""){
		alert("Email or Password cannot be blank.");
		return;
	} 
	if(registerPasswordValue.length < 8){alert("Password length must be 8 or longer")
		return;
	}
	// SAVE REGISTER DATA
	registerEmail = registerEmailValue;
	registerPassword = registerPasswordValue;

	// HIDE REGISTER DIV
	const registerScreen = document.getElementById("registerScreen");
		registerScreen.classList.remove('d-flex');
		registerScreen.classList.add('d-none');
	const loginScreen = registerScreen.nextElementSibling;
		loginScreen.classList.remove("d-none");
		loginScreen.classList.add("d-flex")
})


// LOGIN SCREEN
const loginBtn = document.getElementById("loginBtn")
loginBtn.addEventListener('click', function(){
	const loginEmailValue = document.getElementById('loginEmail').value;
	const loginPasswordValue = document.getElementById("loginPassword").value;

	if(loginEmailValue === "" || loginPasswordValue === ""){
		alert("Email or Password cannot be blank.");
		return;
	} 
	
	loginEmail = loginEmailValue;
	loginPassword = loginPasswordValue;
	

	// SHOW SUCCESS IF LOGIN AND REGISTER MATCHED
	if(loginEmail === registerEmail && loginPassword === registerPassword){
	

	// HIDE LOGIN SCREEN
	const loginScreen = registerScreen.nextElementSibling;
		loginScreen.classList.remove("d-flex");
		loginScreen.classList.add("d-none");

	// SHOW SUCESS SCREEN
	const successScreen = registerScreen.nextElementSibling.nextElementSibling;
		successScreen.classList.remove("d-none");
		successScreen.classList.add("d-flex");

	}



	// SHOW INVALIDATION IF MISMATCHED
	else{

	// HIDE LOGIN SCREEN
	const loginScreen = registerScreen.nextElementSibling;
		loginScreen.classList.remove("d-flex");
		loginScreen.classList.add("d-none");

	// SHOW SUCESS SCREEN
	const failScreen = registerScreen.parentElement.lastElementChild;
		failScreen.classList.remove("d-none");
		failScreen.classList.add("d-flex");

	}
})
